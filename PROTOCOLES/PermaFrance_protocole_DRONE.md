Les images drone ne sont stockées que sur les DDE (pas sur le serveur) : `/media/xavier/PERMAFRANCE_DATA_2TO_2/DATA_BIG`

Manip pour déplacer les .DNG et .JPG dans les dossiers corrects

```sh
$ cd /media/xavier/PERMAFRANCE_DATA_2TO/DATA_BIG/ISERAN/20_JUILLET_2023/DRONE/
$ mkdir JPG
$ mkdir DNG
mv *.JPG JPG/
mv *.DNG DNG/
```


Manip pour créer le fichier texte qui liste toutes les images :
`/media/xavier/PERMAFRANCE_DATA_2TO_2/PermaFRANCE/CAMPAGNES/2023/ISERAN/20_JUILLET_2023/DRONE/contenu_dossier.txt`

```sh
$ cd /media/xavier/PERMAFRANCE_DATA_2TO/DATA_BIG/ISERAN/20_JUILLET_2023/
$ tree -ha > ~/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/DONNEES/20_JUILLET_2023/DRONE/contenu_dossier.txt
```

