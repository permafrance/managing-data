# MEMENTO pour l'organisation et la gestion des terrains

PermaFrance-Glaciers rocheux

> Xavier Bodin, chargé de recherche CNRS, laboratoire EDYTEM [xavier.bodin\@univ-smb.fr](mailto:xavier.bodin@univ-smb.fr) +33-4-79-75-94-43 / +33-6-38-16-75-57

## 0 - RESTE À FAIRE

### ordi

-   refaire cartes localisation avec derniers points de mesures dans les dossiers **`CARTO`** ou **`LOCALISATION`**
-   compléter les dossiers descriptifs de chaque site :
    -   carte localisation Hillshade + orthoimage
    -   carte localisation GPS et UTL actualisée
    -   indications :
        -   temps de trajet
        -   personnes ressources

### matos

#### GPS/R8s

Dans chaque projet :

-   rentrer les pt GPS années précédentes
-   rentrer toutes les ortho + scan25 + DEM + hillshade
-   rentrer les coordonnées L93 de toutes les bases (attention au système de référence d'altitude)

Voir si possibilité de programmer un autre mode d'acquisition :

-   base-mobile sans radio avec post-traitement
-   mobile avec liaison GSM (éventuellement 2 mobiles)
-   mobile seul
-   base seule

Vérifier la longueur des tiges (grise alu + jaune topo) utilisées pour l'antenne de base + marquer les tiges avec leur longueur

#### Drone

Check connection commande \<-\> drone

Copier les ortho et scan (idem que pour GPS) pour chacun des sites

#### Tablette

Il s'agit de la tablette PermaFrance de P. Schoeneich (PACTE) ou de l'ordi terrain Asus d'EDYTEM

copier les dossiers CARTO ou LOCALISATION de chaque site

## 1 - À PREVOIR À L'AMONT

### Général

-   récupérer les dossiers imprimés avec localisation des points GPS et des loggers
-   prévoir la tablette terrain (PACTE-IUGA), à coordonner avec Philippe Schoeneich ([philippe.schoeneich\@univ-grenoble-alpes.fr](mailto:philippe.schoeneich@univ-grenoble-alpes.fr), +33-6-31-78-75-77 )

blabla

### Loggers (MTD)

#### Vérif dernier déchargement des données et changement batterie

Voir pour ça le fichier [GESTION_GST/GST_GESTION_CAPTEURS.ods](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST/GST_GESTION_CAPTEURS.ods)

#### Besoin matos (à coordonner avec P. Schoeneich)

-   tablette PermaFrance / PC terrain Asus, avec câble USB
-   piles neuves pour UTL-3
-   petit tournevis (pour dévisser le bouchon)
-   qqes m de corde à linge pour accrocher logger (ou changer le cable si abimé)
-   noter les loggers à décharger et/ou dont la batterie est à changer : voir fichier [GST_GESTION_CAPTEURS.xls](/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GESTION_GST/GST_GESTION_CAPTEURS.xls)

### Levé GPS

#### Matos

-   à EDYTEM :
    -   soit paire Trimble Geo7X + Topcon GB1000
        -   pour le Topcon, possibilité de brancher une petite batterie externe (type moto, \~ 7-12 Ah), avec pinces croco, en plus des 2 batteries internes, ou la batterie du R8S = autonomie 2 jours
    -   soit GPS RTK Trimbler R8s
        -   batterie externe exprès (une journée d'autonomie) pour le système radio + base
        -   mode base-mobile
        -   Implanter sur points de l'année précédente
-   petit matos :
    -   tige filetée alu pour installer l'antenne de base sur les spits (sacoche bleue turquoise)
    -   tube peinture
    -   éventuellement marteau + burin à pic (pour remplacer/compléter la marque par un poinçon)

#### Soft

-   rentrer les coordonnées de l'année précédente :
    -   GPS R8s connecté en USB-c sur ordi, accès à l'espace de stockage
    -   formater les données tableau en :

| site       | id  | point | type   | x_L93      | y_L93       | z_RAF09  |
|------------|-----|-------|--------|------------|-------------|----------|
| Laurichard | LAU | A1    | mobile | 968135.050 | 6442002.105 | 2294.065 |
| Laurichard | LAU | F1    | fixe   | 968135.050 | 6442002.105 | 2294.065 |

##### Trimble Access dans TSC5 (controleur du R8s)

-   dans projet Site, créer nouvelle étude SITE_DDMMYY (nom du site avec date de la mission)
-   passer par Etudes -\> Propriétés -\> Gestionnaire de couches -\> Fichiers de point pour afficher les ortho et scan25 + MNT hillshade
-   rentrer les ortho et scan25 + MNT hillshade
    -   passer en format GeoTIFF
    -   sur Trimble Access, passer par Etudes -\> Propriétés -\> Gestionnaire de couche -\> Fichiers de carte

### Levé drone

### Levé LiDAR terrestre

### Contacts

| site | personne | qualité | mail | téléphone | remarque |
|----|----|----|----|----|----|
| LOU | Jean-Christophe Gagnière | responsable remontées méca Valcenis | [jcgagniere\@valcenis.ski](mailto:jcgagniere@valcenis.ski) | 06-67-62-88-31 | remplace D. Petit |
| LOU | François-Luc Cimelière | RTM 73 (PAPROG) |  |  |  |
| LAURICHARD | Cyril Coursier | garde du PN Ecrins | [cyril.coursier\@ecrins-parcnational.fr](mailto:cyril.coursier@ecrins-parcnational.fr) | 06-67-71-87-84 | prévenir qqes semaines à l'avances |
| DEROCHOIR | JM Croizat | resp. CTMB |  | 06-12-68-22-51 |  |
| DEROCHOIR | Valérie Paul | resp. CTMB adjointe | [valerie.paul\@compagniedumontblanc.fr](mailto:valerie.paul@compagniedumontblanc.fr){.email} | 06-14-72-54-12 |  |
| CAYOLLE | J. Mansons | chargé mission PN Mercantour | [jerome.mansons\@mercantour-parcnational.fr](mailto:jerome.mansons@mercantour-parcnational.fr) | 06-99-02-89-84 |  |
| CAYOLLE | Ludovic Klein | chef d'antenne Ubaye | [ludovic.klein\@mercantour-parcnational.fr](mailto:ludovic.klein@mercantour-parcnational.fr) | Tel : +33 (0)4 92 81 21 31 - Mobile : +33 (0)6 34 47 67 81 |  |
| CAYOLLE | Laurent MARTIN-DHERMONT | Garde-Moniteur - Technicien de l'environnement / Vallée de l'Ubaye, PN Mercantour | [laurent.martindhermont\@mercantour-parcnational.fr](mailto:laurent.martindhermont@mercantour-parcnational.fr) | 07-63-58-12-72 |  |
| FARNEIRETA | Anne Goussot | chargée mission Biodiv, PNR Queyras | [annegoussot\@yahoo.fr](mailto:annegoussot@yahoo.fr) ou [a.goussot\@pnr-queyras.fr](mailto:a.goussot@pnr-queyras.fr) |  | retour au PNRQueyras début 2023 ? |
| FARNEIRETA | Pierpaolo Brena | PNR Queyras | [p.brena\@pnr-queyras.fr](mailto:p.brena@pnr-queyras.fr) |  | quitte le PNRQ fin 2022 ? |
| FARNEIRETA | Jean-Baptiste Portier | Animateur Natura 2000 PNR Queyrras | [jb.portier\@pnr-queyras.fr](mailto:jb.portier@pnr-queyras.fr) |  |  |
| ISERAN |  |  |  |  |  |

## 2 - À FAIRE AU RETOUR

### Renseignements sur la mission

Dans `/PermaFrance/CAMPAGNES/ANNEE/SITE/DATE`, éditer le .md avec: `date`, `participant·e·s`

Dans `/PermaFrance/DONNEES/1_DATA_MANAGEMENT/PermaFrance_sites.ods`,

Dans `/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GESTION_GST/GST_GESTION_CAPTEURS.xls`
