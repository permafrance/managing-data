# PermaFrance - Description of the Rock Glacier Velocity (RGV) protocols

> This is a complementary document to the information found in the following folders:
>
> -   [/PROTOCOLES/1_ACQUISITION](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/1_ACQUISITION)
> -   [/PROTOCOLES/2_TRANSFERT](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/2_TRANSFERT)
> -   [/PROTOCOLES/3_TRAITEMENT](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/3_TRAITEMENT)

The present documentation is a local implementation (for the French observatory of permafrost - PermaFrance) of the documents produced by the IPA Action group Rock Glaciers Inventories and Kinematics. It contains practical information as well as technical details aimed at allowing the production of the Rock Glacier Velocity data.

The basis documents of the Task 1 of the RGIK *"Kinematics as an optional attribute in standardized rock glacier inventories"* are the following:

-   [Baseline concepts (v3.0.1) [22.07.2022]](https://bigweb.unifr.ch/Science/Geosciences/Geomorphology/Pub/Website/IPA/CurrentVersion/Current_KinematicalAttribute.pdf)

-   [Practical guidelines: rock glacier inventory using InSAR (kinematic approach) (v3.0.2) [11.06.2020]](https://bigweb.unifr.ch/Science/Geosciences/Geomorphology/Pub/Website/CCI/CurrentVersion/Current_InSAR-based_Guidelines.pdf)

## 1. Acquisition step

This step refers to the time when measurements are carried out on the field, with a dedicated GNSS device handled by one or two operators. Each site has a specific sampling design (number of marked blocks, distribution in space), initially set up and constrained by the terrain and by the necessity of achieving the GNSS in a single day.

### 1.1 Description of the devices

Within the PermaFrance groups, we use the following Differential GNSS devices[^permafrance_protocole_rgv_rock_glaciers_velocity-1] :

[^permafrance_protocole_rgv_rock_glaciers_velocity-1]: add links to technical specification and manuals of the devices

-   at EDYTEM: Topcon GB1000 (as a base station) + Trimble GEO7X (as a rover) since \~2010 + a pair of Trimble R8s since 2022 (as a RTK)
-   at PACTE: 2 x Trimble GEO7X (base + rover)

As shown on the photo below, a 2-m pole and a bipode are generally used for the rover, and the target is materialized on the ground with a painted cross, whereas the point of the base station est composé d'une tige filetée diam. 8mm spitée dans la roche et sur laquelle se visse une pièce raccord "maison" en alu (longueur = 130 mm = 0.13 m) pour y fixer l'antenne.

![*On the left: The PG-A1 antena of the Topcon GB1000 base station installed on an alu 13-cm pole with a 5/8' geodetic thread. On the right: pole and bipode for measuring points with a Trimble Geo7x GNSS rover.*](RGV_GNSS_field_acquisition_2000px.jpg)

### 1.2 Collecting data

Annual field campaign, generally done in late summer for ensuring the absence of snow (that may cover marked blocks).

Around 10 to 50 (depending on the site and the mood of the one that initially setup the survey) marked blocks are measured during each campaign, using a base station (`/PermaFrance/DONNEES/1_DATA_MANAGEMENT/COORDONNEES_BASE_GPS.txt`) and a rover station (2-m high pole + bipode).

#### Collecting data with Trimble Geo7X + Topcon GB1000
See [these guidelines](https://gricad-gitlab.univ-grenoble-alpes.fr/permafrance/managing-data/-/blob/1dc7937f954f4a3f17afa9fde65fdd5b6410f62c/PROTOCOLES/1_ACQUISITION/GPS/Geo7X_GB1000.md)


#### Collecting data with a pair of Trimble Geo7X

#### Collecting data with Trimble R8S (RTK)

##### RTK mode ("base-mobile")

##### Post-processing ()

## 2. Processing phase

### 2.1 From the field to the hard-drive

#### Procedure with Topcon GB1000 GNSS

Voir [ce petit tuto](/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GPS/TOOLS/DECHARGER_GB1000_DEPUIS_PC_UBUNTU.txt):

1.  Raw files stored in `PermaFRANCE/CAMPAGNES/YYYY/SITE/DD_MONTH_YYYY/GPS`?
2.  

#### Procedure with Trimble Geo7X GNSS

-   if the Geo7X is not connectable to the PC with PathFinder:
    -   use the SD Card in the Geo7X to transfer the data (formats `*.dd`, `*.gic`, `*.gip`, `*.gis`, `*.giw`, `*.gix`, `*.obs`, `*.obx`)
    -   with PathFinder, convert those files to `*.ssf*`
-   if the Geo7X can be connected to the PC with PathFinder

#### Procedure with Trimble R8S

On accède aux données en connectant au PC le controleur TSC5 par l'USB-c : quand on le connecte, un message s'affiche sur l'écran du TSC5 "Préférences USB" = "Utiliser la connection USB pour (×) Transfert de fichier". Toutes les données du projet et de l'étude se trouvent dans `mtp://Trimble_TSC5_JAJ213500385/Espace%20de%20stockage%20interne%20partag%C3%A9/Trimble%20Data/Projects` (pour un PC sous Ubuntu) auquel on accède normalement avec l'explorateur de fichiers du PC

-   for "Base-Mobile" levé : exporter Fichier ESRI shape = bouton en bas du menu de l'étude
-   pour "post-traitement" : Menu -\> Instrument -\> Fichiers récepteur (connecté en bluetooth) -\> Importer du récepteur (choisir les fichiers T02 en fonction de leur date)

### 2.2 From the hard-drive to the post-processed data

#### GB1000

Rajouter \*.tps au fichier si absent

Avec **Topcon Link**, sur PC GPS : conversion de format Topcon à Rinex

### 2.3 From the post-processed data to the time series/portal diffusion
