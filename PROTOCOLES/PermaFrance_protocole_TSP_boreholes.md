# PermaFrance - Description of the Ground Surface Temperature (GST) protocol



## 1. Acquisition (of raw data)

### 1.1 Device

Miniature Temperature Datalogger (MTD) [UTL-3](https://www.geotest.ch/en/innovation/utl-temperature-datalogger)

![Un MTD installé à la Cayolle](/home/xavier/DATA/ALPES/SITES/ALPES_SUD/UBAYE/CAYOLLE/PHOTO/PHOTOS_POINTS_GPS/GPS_15_2_logger_CAY1_crop.jpg)


#### Technical specifications

Memory = 

Battery = 

...



### 1.2 Collecting data
Unscrew the tap of the MTD, and connect it with a  USB/mini USB wire to the PC
Software ??? -> Download data (ou un truc du genre)


/!\\ quel logiciel ? Quel type de câble USB ?



## 2. Processing

### 2.1 From the field to the hard-drive

1. Raw files (`*.csv, *.txt, *.utl`) are stored in `PermaFRANCE/CAMPAGNES/YYYY/SITE/DD_MONTH_YYYY/MTD`
2. Intermediary data format refinement (if need suppress errors, change field separator)
3. Fill the information in `PermaFRANCE/BD/1_DATA_MANAGEMENT/METADATA_GST_FILES.ods`:

| FILE  | FOLDER | FIELD_SEP | other info |
| -------------- | ------------------------------------------- | -------------- | ------------------------------------------------------------ |
| `filename.csv` | `PermaFrance/DATA/CAMPAGNES/20XX/SITES/GST` | `;` or `,` or `\t` |                                                              |

**This table is IMPORTANT: it links each data file to the `R` scripts that allow managing the GST time series.**



	The raw data files are stored in: 
	`PermaFRANCE/CAMPAGNES/YYYY/SITE/DD_MONTH_YYYY/MTD` 

Penser à nettoyer les fichiers brutes stockés dans `PermaFRANCE/DATA`



### 2.2 From the hard-drive to the time series

Scripts à partir desquels il faut travailler pour arriver à ZE script qui ne bouge plus:

- `/home/xavier/DATA/ALPES/SITES/ECRINS/LAURICHARD/MTD/RGL1_GST/Script_R/SCRIPT_MANIP_GST.R`
- `/home/xavier/DATA/TRAITEMENTS/R/Work/LAURICHARD/Laurichard_explore_gst.R`
- `/home/xavier/DATA/TRAITEMENTS/R/Work/LAURICHARD/Laurichard_explore_velocity.R`
- `/home/xavier/DATA/TRAITEMENTS/R/Work/LAURICHARD/Laurichard_GST.R`
- `/home/xavier/DATA/TRAITEMENTS/R/Work/LAURICHARD/Laurichard_GST_gestion.R`
- `/home/xavier/DATA/TRAITEMENTS/R/Work/LAURICHARD/Laurichard_GST_gestion_021216.R`
- `/home/xavier/DATA/ALPES/SITES/ALPES_SUD/UBAYE/CAYOLLE/MTD/manip_data_mtd_cayolle.rmd`





### 2.3 From the time-series to the web portal

blab