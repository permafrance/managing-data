# GNSS - Trimble Geo7X (mobile) + Topcon GB1000 (base) - acquisition guidelines

## Presentation of the equipment

### Topcon GB100 (used as the base station)

Equipement typé géodésie, relativement ancien (acheté avant 2010), qui tient dans la petite sacoche bleue (20 x 20 x 20 cm), pour un poids \< 2kg, composé de :

-   un récepteur simple et robuste, avec peu de touches, un écran LCD de taille réduite, peu de ports ;

-   une antenne (modèle PG-A1), jamais calibrée ou testée mais qui semble toujours fonctionner correctement ;

-   un câble reliant l'antenne au récepteur ;

-   un chargeur (avec câble courant, petit électroménager) pour les deux batteries du récepteur ;

-   des câbles divers, dont un avec pinces "croco"" permettant d'alimenter le récepteur avec une batterie externe.

Pour le déchargement des données, la connexion à l'ordi peut se faire de différentes façons, mais celle privilégiée passe par un câble Ethernet : fonctionnement par protocole FTP ([/GESTION/GESTION_GPS/TOOLS/DECHARGER_GB1000_DEPUIS_PC_UBUNTU.txt](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GPS/TOOLS/DECHARGER_GB1000_DEPUIS_PC_UBUNTU.txt) ) ou bien logiciel PCCDU, sur Windows ([/GESTION/GESTION_GPS/TOOLS/trucs IP.txt](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GPS/TOOLS/trucs%20IP.txt)).

### Trimble Geo7X (used as the rover)

Equipement acheté en 2014 (?) plutôt typé pour la cartographie, qui

## What should be checked before going on the field

### Chargement des batteries

### Etat de la mémoire

## The procedure on the field
