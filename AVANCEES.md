# Feuille de route pour la gestion des données du réseau d'observation PermaFrance


## 7 janvier 2025
### Stage Paul Gorrias

Démarre aujourd'hui, avec mise en place d'un dossier du serveur sur lequel il va bosser :
`/run/user/1000/gvfs/dav:host=nextcloud.osug.fr,ssl=true,prefix=%2Fremote.php%2Fdav%2Ffiles%2Fbodinx/PermaFRANCE/DIVERS/STAGE_GORRIAS_2025`, avec seulement des données concernant le GPS (en gardant la structure du dossier initial dans `/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data`).


## 20 décembre 2024
### tableau sites
`/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/PermaFrance_sites.ods`

 **Actualiser le GPKG `/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/sites_permafrance_sno2021.gpkg`**


### Dérochoir
Rajouté données UTL 2024 Philippe

## 19 octobre 2024

### Architecture PGD / projet RStudio

-   **Est-ce que la subdivision en dossiers `ACQUISITION` / `TRANSFERT` / `TRAITEMENT` dans `/PROTOCOLES` est ok ?**
-   En tout cas, **j'ai supprimé `/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/SCRIPTS`** : autant tout mettre dans `/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES` et les trois dossiers (***à voir à la pratique***)

### Manip à faire pour les campagnes

#### Dérochoir

-   **données GPS** prises avec les 2 Geo7X de l'IUGA ***à récupérer et à post-traiter***
-   **données UTL** relevées par Philippe ***à récupérer***

#### Fournache

-   **données GPS** prises avec Geo7X + GB1000 ***à récupérer et à post-traiter***
-   **données UTL** relevées par Philippe ***à récupérer***
-   **images drone** de Sylvain Jobard ***à récupérer (et à traiter ?)***

#### Farneiréta

-   **données GPS** : vérifier les hauteurs d'antenne : cf dZ = 0.14 pour pt controle 2020-2021 ?!!

#### Cayolle

-   ***compléter le CR***
-   

#### Lou

-   

#### Laurichard

### Manips générales à faire

#### UTL / GST

-   **remplir le tableau** [GST_GESTION_CAPTEURS.ods](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST/GST_GESTION_CAPTEURS.ods)
    -   tableau pour générer les graph synoptiques de suivi 1) des déchargements UTL et 2) des changements de batterie
    -   pas claire l'utilisation du tableau
    -   coordonnées XYZ des UTL :
        -   pas nécessaire ici ?
        -   avoir un autre fichier ?
        -   dater ces coordonnées
-   **remplir le tableau** [GST_METADATA.ods](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST/GST_METADATA.ods)
    -   une ligne par fichier de données brutes
    -   utilisé pour la production des séries temporelles

#### GPS

-   **Vérifier et compléter tableau de suivi**

    -   [PermaFrance_campagnes_GPS.ods](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/PermaFrance_campagnes_GPS.ods)

-   **campagnes R8S à traiter :**

    -   `/PermaFRANCE/CAMPAGNES/2022/CAYOLLE/10_OCTOBRE_2022/GPS` (R8S non RTK =\> avec Trimble Office machin)

-   **campagnes Geo7X + GB1000 à traiter :**

    -   `/PermaFRANCE/CAMPAGNES/2017/LOU/12_AOUT_2017/GPS`

-   **campagnes Geo7X + Geo7X à traiter :**

    -   `/PermaFRANCE/CAMPAGNES/2018/BELLECOMBE/05_OCTOBRE_2018/GPS`

-   **campagnes Promark à traiter :**

    -   `/PermaFRANCE/CAMPAGNES/2013/DEROCHOIR/13_SEPTEMBRE_2013/GPS`
    -   `/PermaFRANCE/CAMPAGNES/2015/DEROCHOIR/14_OCTOBRE_2015/GPS`

-   **compilation à faire :** ***après vérification : HAE ou RAF09 ? Base OK ?***

    -   `/media/xavier/PERMAFRANCE_DATA_2TO_GRENOBLE/PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/CAYOLLE/MOUVEMENT/COMPILATION_UNIQUE.ods`

-   **calcul vitesses :**

    -   à partir de `/PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/CAYOLLE/MOUVEMENT/Cayolle_explore_velocity.R`
    -   et qui permet de sortir `/PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/CAYOLLE/MOUVEMENT/rg_cayolle_topo_lambert93_velocity.shp`

## 14 octobre 2024

Fichier `/GESTION_FORAGES/DeuxAlpes_Gestion_Maintenance.xls` récupéré de `/media/xavier/PERMAFRANCE_DATA_2TO_GRENOBLE/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT` et remis dans `/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_FORAGES` (***pas forcément la bonne place...***)

## 30 septembre 2024

Revoir le fichier `/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST/GST_GESTION_CAPTEURS.xls` avec actualisation des dates de changements de pile, état des capteurs, ID des logger...

## 25 juillet 2024

### RESTE À FAIRE

#### Gestion fichiers données GST

Comparaison des différents tableaux de métadonnées : - `/media/xavier/PERMAFRANCE_DATA_2TO_EDYTEM/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST` - `davs://nextcloud.osug.fr/remote.php/dav/files/bodinx/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST`

Y compris dans le dossier `OLD`.

=\> PS a fait un tri et a harmonisé les tableaux de suivi de batterie et relevé/chargement/déchargement des loggers : fichier normalement OK = `/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/GESTION_GST/GST_GESTION_CAPTEURS_PS-18-7-2024.xls` (sur DDE EDYTEM et sur Nextcloud)

## 23 mai 2024

Pas mal de boulot de synchro/copié-collé entre DDE (mais pas avec Nextcloud), voir le `journal_***.md` à la racine du DDE EDYTEM

Qqes notes sur mon agenda, sur les ***choses à faire*** :

1.  reprendre synchro entre DDE_EDYTEM et Nextcloud

2.  harmonisation des noms de sites et dossiers

3.  ...

4.  Travail de définition / dessin du workflow

## 26 janvier 2024

Dans `/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/README.md`, j'ai repris les parties 1 et 2, mais pas encore la partie 3 (structure et gestion des données)

## 21 juillet 2023

Création des dossiers : [/PROTOCOLES/1_ACQUISITION](/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/1_ACQUISITION) [/PROTOCOLES/2_TRANSFERT](/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/2_TRANSFERT) [/PROTOCOLES/3_TRAITEMENT](/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/3_TRAITEMENT) afin de détailler les procédures selon les phases de gestion des données

## 20 juillet 2023

### Restructuration fichiers

`README.rmd` et `AVANCEES.rmd` séparés pour ne pas alourdir le 1er

## 19 juillet

Branche main : gros ménage, sur fichiers et dossiers inutiles, avec une sauvegarde dans `/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/gitlab_copie_branch_main_19juillet_2023`

Merge request en cours... =\> ok, done avec succès, avec partir de l'interface en ligne.. pfiou je respire ;-)!

## 27 juin

### Stage Alexis

Point sur avancement du transfert des données brutes de `1_DATA` vers `CAMPAGNE`

A noter qu'il y a des données GPS brutes et post-traitées qui peuvent traîner sur : - ordi PC EDYTEM Carto/GPS - NAS (disque dure réseau) Xavier `afp://xbodi@CTI4D-NAS-2To.local/xbodi/DATA/TRAITEMENTS/GPS/PROJECTS` \* pour y accéder, mettre dans barre adresse de nemo/explorateur l'un de ces chemins : \* [afp://cti4d\@CTI4D-NAS-2To.local/cti4d/](afp://cti4d@CTI4D-NAS-2To.local/cti4d/){.uri} \* [afp://xbodi\@CTI4D-NAS-2To.local/xbodi/](afp://xbodi@CTI4D-NAS-2To.local/xbodi/){.uri} \* [afp://xbodi\@192.168.169.204/xbodi](afp://xbodi@192.168.169.204/xbodi){.uri} \* [afp://cti4d\@192.168.169.204/cti4d](afp://cti4d@192.168.169.204/cti4d){.uri}

#### Pour accéder aux données sur le NAS

-   j'ai créé, à partir de la page <http://192.168.169.204/?locale=fr> un utilisateur : `alexis_m` / mdp = `alexis`
-   

## 13 juin - Echanges avec Alexis

### Stage Alexis

Point sur le remplissage du tableau

Démarrage chantier déplacement des données brutes GPS (de 1_DATA vers CAMPAGNES), avec tableau de suivi

Déplacement du dossier Tableaux vers `C:\Users\LENOVO\Desktop\STAGE XAVIER\GITLAB\managing-data\GESTION\stage_alexis`

Voir sa branche (qu'il faudrait sans doute merger prochainement... ?)

Mise en place de l'accès au Nextcloud : il a désormais un compte, et on a mis en place sur son ordi l'accès serveur depuis l'explorateur de fichier (**attention aux fausses manip**)

### Mise à jour données topo Laurichard

-   copie de `rg_lau_topo2022.csv` sur `PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/LAURICHARD/MOUVEMENT/BRUTES/`
-   mise à jour de `PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/LAURICHARD/MOUVEMENT/DATA/rg_laur_topo_NA_lambert3.ods` avec les données 2022

## 5 juin 2023 - 1ers échanges avec Alexis

Explication sur l'architecture de la gestion des données PermaFrance

1ers pas d'Alexis : c'est tout clé SSH ok et projet cloné

## Au 2 juin 2023 - F. Pérignon (Gricad), F. Magnin

Aide de F. Pérignon : - explication clé SSH - clone du projet de Florence, en passant par RStudio (New project) -\> le machin est fonctionnel pour Florence

**==\>\> C'est bon, on peut démarrer !**

## Au 11 mai 2023 - discussion avec Cécile (et un peu Florence)

### à faire

-   avancer sur le schéma conceptuel de base (***sur un tableau blanc***) :

-   avancer sur les fiches protocoles

### Autres

-   conseille d'amorcer les discussions avec GlacioClim sur cette base (protocoles)
-   mention du montage en cours, par C. Pellet (Permos), d'un projet COST sur la gestion des données permafrost

=\> on va tâcher d'avancer sur les deux tableaux :

-   moyens/finances/personnels à l'échelle nationale, notamment avec GlacioClim et SNO
-   gestion données / réseaux à l'échelle internationale, avec notamment initiative COST en cours + GTN-P + IPA Action group RGIK

*=\> on se revoit le 23 mai avec Flo pour avancer sur mettre en place Git/RStudio*

## Au 21 mars 2023 - discussion avec Flo et Philippe

Flo est ok pour participer à l'élaboration de cette feuille de route : on se voit le jeudi 11 mai matin pour ça.

D'ici là, le doc pour visualiser la feuille de route est partagé à tout le monde (<https://gricad-gitlab.univ-grenoble-alpes.fr/xbodi/managing-permafrance-data/-/blob/main/README.rmd>)

Et on verra avec Flo si ok pour passer par R & Gitlab (ça implique d'avoir un compte sur <https://gricad-gitlab.univ-grenoble-alpes.fr/users/sign_in>)

## Au 14 mars 2023 (discussion avec CP)

### Rappels

-   Gitlab `https://gricad-gitlab.univ-grenoble-alpes.fr/xbodi/managing-permafrance-data/` = portail interne pour la gestion des données

On vise d'avoir, in fine (horizon 4-5 ans), un portail type Shiny (permos.ch) =\> en lien avec les discussions à avoir avec Glacioclim =\> prendre sans trop tarder contact avec Antoine R et Delphine S, au moins pour savoir quelle est leur démarche d'une façon générale

-   Gestion des données GlacioClim

### Trame des choses à faire

#### Fiches protocoles

Voir `/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES` - PermaFrance_protocole_GST_surface_temperature.md - PermaFrance_protocole_RGK_rock_glaciers_kinematics.md - PermaFrance_protocole_TSP_boreholes.md

-   protocoles terrain : `/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/GESTION_TERRAINS.md`

=\> à voir si fusuionner les deux

#### Stagiaire

Prendre le temps de réfléchir

#### Discussions

En interne PermaFrance : Flo ?

Avec GlacioClim

#### Tableau de synthèse des sites suivis

## Au 12 janvier 2023

### Création d'un fichier de planification

[Gantt](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/Gantt_PermaFrance.planner)

## Au 2 décembre 2022

### Discussion du 28 nov 2022 avec OSUG

Voir [Notes_reu_CP_XB_28NOV2022.md](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFrance/Notes_reu_CP_XB_28NOV2022.md)

Fabien Malbet et Bernard Boutherin nous ont clarifiés, à Cécile et Xavier, les possibilités en lien avec l'OSUG, et on a convenu avec eux :

\- on avance sur notre Plan de Gestion des Données

\- on voit ensuite si les outils de l'OSUG (Geoflow/Geonetwork, ou Shiny) peuvent servir

### Discussion du 29 nov 2022

Cécile et Xavier

Pour le PGD :

\- une "mindmap" sur Miro : <https://miro.com/app/board/uXjVP-rSV9U=/?share_link_id=970020178107>

\- le document sur Opidor: <https://dmp.opidor.fr/plans/10244>

### Atelier Git & R du 1er décembre 2022

Pas appris grand chose, mais au moins ça m'a relancé ! Projet sur Gitlab de Gricad : <https://gricad-gitlab.univ-grenoble-alpes.fr/xbodi/managing-permafrance-data>

***A faire : voir avec PERMOS la possibilité de nous partager leur Gitlab*** *yes, very important*
