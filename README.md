# 00 - Avant de commencer

Les infos du README initial (de Gitlab) sont dans [README_init.md](https://gricad-gitlab.univ-grenoble-alpes.fr/xbodi/managing-permafrance-data/-/blob/main/README_init.md).

Lien du projet Gitlab : <https://gricad-gitlab.univ-grenoble-alpes.fr/xbodi/managing-permafrance-data>

Ce README vise à **formaliser la façon dont les données PermaFrance sont gérées**, notamment au travers :

-   du serveur de données OSUG - PermaFrance
-   du site web PermaFrance
-   d'une série de routines `R` et de protocoles écrits

L'ambition est d'avoir une gestion des données PermaFrance qui satisfasse des standards reconnus et permette de distribuer les données d'observation du permafrost de montagne en France, tout en prenant en compte les différentes contraintes (humaines, matérielles, écologiques) que rencontre(ra) l'approche.

Pour le moment (juin 2022), l'ensemble de la démarche visée par ce README est limitée au temps de travail d'une seule personne, qui n'y consacrera a priori que 15-30 % de son temps de travail maximum dans les mois à venir...

## Répertoire suivi avec le projet Gitlab OSUG "Managing PermaFrance Data"

`~/PermaFrance/DONNEES/1_DATA_MANAGEMENT/CODES/GITLAB_OSUG/managing-permafrance-data`

Et le fichier `/PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/managing-permafrance-data.Rproj` permet de travailler dans RStudio

## Rappel des commandes Git

``` bash
git status # pour connaître les changements
git pull # pour récupérer les changements
git add nom_fichier OU > git add . # pour ajouter des fichiers à suivre, ou tous les fichiers (".")
git commit nom_fichier -m "commentaires" # pour préciser les changements
git commit -am "commentaires" # pour des commentaires qui concernent tous les fichiers
git push # pour envoyer les changements

gitk # pour visualiser l'arbre des changements
```

Toutes ces commandes sont accessibles dans RStudio, via les boutons dans le menu Git :

![](/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/DIVERS/git_boutons_rstudio.png)



## Objectifs du projet Gitlab

Il s'agit d'établir un **espace de travail unique**, **collaboratif** (parce qu'accessible sur le serveur Gitlab de Gricad) et **versionné** (parce qu'utilisant Git) combinant, pour résumer :

-   des fichiers texte à formatage léger (essentiellement en format *Markdown* `*.md`, éditables dans n'importe quel éditeur de texte, et visualisables en version mise en page, par exemple, avec Typora)
-   des scripts `R` (format `*.R`, ou `*.Rmd` si besoin d'une description détaillée) ou `Python` (ou *Jupyter Notebook*) avec les routines de traitement
-   des liens vers les données stockées sur le serveur OSUG PermaFrance et vers le site web

D'un point de vue plus pratique, les objectifs de ce projet sont les suivants :

-   **Constituer une porte d'entrée pour les personnes gérant les données PermaFrance, pour pouvoir mener les tâches suivantes :**
    -   *Acquisition des données, essentiellement sur le terrain (mais aussi télédétection)*
    -   *Traitement des données acquises lorsque nécessaire*
    -   *Production des données à distribuer*
    -   *Mise à disposition des données (notamment via le site web PermaFrance)*
-   **Etablir les protocoles utilisés pour la gestion des données PermaFrance**
    -   *Ces derniers décrivent l'ensemble des procédure (*workflow*) qui permettent d'acquérir (sur le terrain), de transférer (du terrain au bureau), de traiter (au bureau) et de mettre à disposition les données*
-   **Suivre les missions de terrain**
-   **Maintenir la traçabilité des données et leur complétion (séries temporelles)**
-   **Etre à jour de toutes les modif dans les données stockées sur le serveur et dans les codes**
-   **Propager les modif (codes, métadonnées et données) vers les copies de sauvegarde (DDE)**
-   **S'assurer de la mise à jour des données et des graphiques qui sont distribués via le site web**

## Définitions et détails techniques

-   **données brutes** = *fichiers de données telles que directement produites par le capteur ou le traitement*
-   **données traitées** = *fichiers de données qui ont été obtenues par des traitements (procédures manuelles, routines R, autres procédures spécifiques)*
-   **données distribuées** = *fichiers de données traitées et formatées selon un standard pré-établi et visant à être diffusées par l'observatoire PermaFrance*
-   **protocoles** = *description de la, ou des procédures permettant d'effectuer une ou plusieurs opérations successives ou parallèles et aboutissant à la production des données (brutes, traitées ou distribuées)*
-   **disques durs externes** = *stockage de l'ensemble des données PermaFrance (hors serveur OSUG-DC), y compris des données lourdes non stockées sur le serveurs*
-   **paramètre ECV** = *voir <https://gcos.wmo.int/en/essential-climate-variables/permafrost/>*

------------------------------------------------------------------------

# 1 - Site web [PermaFrance](https://permafrance.osug.fr/)

-   mis en place en juin 2021, peu retouché depuis...
-   migration par l'OSUG depuis [new-permafrance.osug.fr/](https://new-permafrance.osug.fr/)
-   site web qui sera interfacé avec un serveur de distribution des données

## Architecture du site

Le tableau suivant présente les principaux menus et sous-menus du site web :

| Réseau | Sites et mesures | Données |
|--------------------------------|----------------------|------------------|
| C'est quoi le PF de montagne ? | Où fait-on des mesures ? | Données spatialisées |
| C'est quoi PermaFrance ? | Que mesure-t-on ? Comment ? | Séries temporelles |
| Pourquoi faut-il suivre le PF de montagne ? |  |  |

## Données distribuées

Les données distribuées sont soit des **données spatialisées** (type raster ou vecteur) soit des **séries temporelles** (tableaux), accessibles par des *flat files* (à défaut de mieux...) sur les pages ***Données*** du site web. Les données distribuées par le site web sont **stockées et maintenues à jour sur le** ***serveur de données OSUG-DC*** dédié à PermaFrance (voir section 3).

Il ne s'agit pas de données brutes (qui sont aussi stockées sur le serveur), mais de **données préparées à partir de ces dernières**, selon une démarche précisée dans les protocoles et définie au sein de l'observatoire PermaFrance et en concertation avec la communauté scientifique afin de correspondre aux principaux attendus relatifs à ce type de données (notamment pour les données correspondant à des paramètres de l'[ECV Permafrost](http://wmo.org/ecv_ou_un_truc_du_genre)).

L'actualisation des données est variable selon le type de données, mais au mieux une fois par an a priori pour les données acquises par des mesures continues, et au pire jamais (ou très rarement : une fois tous es 10 ans ?!) pour les données spatialisées (qui représentent des phénomènes dont l'évolution n'est a priori pas rapide, du moins pas perceptible par les outils d'acquisition de données dont on dispose).

Du fait qu'elles sont actualisées régulièrement, les séries temporelles demandent le plus d'attention dans la construction de l'interfaçage entre le site web et le serveur. L'idée est a priori de mettre au point des routines `R` permettant d'automatiser et surtout de standardiser différents aspects :

-   incorporation de nouvelles données brutes, et mise à jour des métadonnées
-   test et procédures diverses pour évaluer la qualité et la continuité temporelle des séries
-   génération des données et des graph distribués par le site web

```         
A voir :
    - question des métadonnées
    - DOI
    - doc de description des variables/données distribuées (notamment comment on transforme les données brutes)
```

# 2 - Stockage des données

## 2.1 - Disques durs externes

Toutes les données PermaFrance, ainsi que les documents, scripts, illustrations et documentation diverses sont sauvegardées sur deux disques externes `PERMAFRANCE_DATA_2TO_GRENOBLE` et `PERMAFRANCE_DATA_2TO_EDYTEM`, qui sont maintenus identiques via un utilitaire de sauvegarde et de synchronisation (tel que (FreeFileSync)(<https://freefilesync.org/>)). L'un ou l'autre des DDE peut être amené sur le terrain, et il convient de synchroniser régulièrement les DDE avec le serveur OSUG (via l'outil Nextcloud)

### 2.1.1 - Caractéristiques techniques des DDE :

*Info DDE PERMAFRANCE_DATA_2TO_GRENOBLE au 26 janvier 2024* ![Info DDE PERMAFRANCE_DATA_2TO_GRENOBLE au 26 janvier 2024](https://nextcloud.osug.fr/index.php/s/YnxrqrrecfmjJba/download/PERMAFRANCE_DATA_2TO_GRENOBLE.png "Info DDE PERMAFRANCE_DATA_2TO_GRENOBLE au 26 janvier 2024")

------------------------------------------------------------------------

*Info DDE PERMAFRANCE_DATA_2TO_EDYTEM au 25 juillet 2024* ![Info DDE PERMAFRANCE_DATA_2TO_EDYTEM au 25 juillet 2024](https://nextcloud.osug.fr/index.php/s/eia4CcdpLgBngg7/download/PERMAFRANCE_DATA_2TO_EDYTEM.png "Info DDE PERMAFRANCE_DATA_2TO_EDYTEM au 25 juillet 2024")

Les deux DDE contiennent la structure de répertoires suivante :

```         
├──   DATA_BIG
│    ├──   DONNEES
│    ├──   PERSONNES
│    └──   SITES
├──   PermaFRANCE
│    ├──   ADMIN
│    ├──   CAMPAGNES
│    ├──   DIVERS
│    └──   DONNEES
```

## 2.2 - Serveur de données [Nextcloud](https://nextcloud.osug.fr/index.php/s/fPgJwoNqm7XocMZ)

### Principes

Cet espace de stockage géré et mis à disposition par l'OSUG est **une réplique du dossier `PermaFRANCE` des DDE**. Pour éviter une saturation du serveur et un stockage en ligne énergivore et peu utile, le dossier `BIG_DATA`, contenant par exemple les photos des vols drone, les données LiDAR, les données IGN (ortho, LiDAR HD) et autres données géomatiques utiles et/ou reliées à PermaFrance, n'est pas présent sur le serveur.

### Contenu

# 3 - Structure générale et principes de gestion

## 3.1 - Le dossier `/DATA_BIG` (uniquement sur les DDE)

Le dossier `DATA_BIG` contient des données dont le volume ne permet/justifie pas le stockage sur le serveur de l'OSUG : il n'est donc stocké et sauvegardé que sur les deux DDE.

Contenu :

```         
├── [   0]  DONNEES
│   ├── [8.0K]  Documents_contenu_tablette_IUGA_PermaFrance
│   ├── [4.0K]  INVENTAIRE_GR_ALPES_FRANCE
│   └── [4.0K]  ORTHO_IGN
├── [4.0K]  PERSONNES
│   ├── [   0]  ALEXIS_MAYEUX
│   ├── [4.0K]  MARCO_MARCER
│   ├── [   0]  NINO_BERNE
│   ├── [   0]  THIBAUT_DUVANEL
│   ├── [4.0K]  THOMAS_ECHELARD
│   └── [   0]  XAVIER_BODIN
└── [4.0K]  SITES
    ├── [4.0K]  AUTRES_SITES
    ├── [   0]  CAYOLLE
    ├── [   0]  DEROCHOIR
    ├── [   0]  FARNEIRETA
    ├── [   0]  ISERAN
    ├── [   0]  LAURICHARD
    └── [4.0K]  LOU
```

## 3.2 - Les procédures de synchronisation

On (Xavier B) utilise FreeFileSync et Nextcloud, et avec maintien des journaux :

-   `/media/xavier/PERMAFRANCE_DATA_2TO_GRENOBLE/PermaFRANCE/Notes_mises_a_jour.md` ***(dernière modification : 14 oct. 2022)***
-   `/home/xavier/DATA/RESEAUX/PERMAFRANCE/BD_OSUG_DC/PermaFRANCE/Notes_mises_a_jour.md` ***(dernière modification : 20 juil. 2023)***
-   `davs://nextcloud.osug.fr/remote.php/dav/files/bodinx/PermaFRANCE/Notes_mises_a_jour.md` ***(dernière modification : 27 sept. 2023)***

## 3.3 - Les dossiers synchronisés

-   `PermaFRANCE/ADMIN` : tout ce qui permet de gérer l'observatoire d'un point de vue administratif et financier

-   `PermaFRANCE/CAMPAGNES` : tout ce qui concerne les campagnes de mesure, incluant ce qui à l'amont permet de préparer la campagne (cartes...), les données collectées sur le terrain et les données éventuellement traitées / post-traitées

-   `PermaFRANCE/DIVERS` : contient des choses variées, pas utiles pour le moment mais qui le seront peut-être plus tard

-   `PermaFRANCE/DONNEES` : tout ce qui permet de produire et distribuer les données finales. Structuré en deux dossiers : `1_DATA_MANAGEMENT`, `2_DATA`

```         
A voir :
    - quelle procédure de synchronisation (`rsync` ?) et selon quel protocole ? => à affiner avec les collègues informaticien·ne·s de l'OSUG-DC
    - rsync, outil Nextcloud, Freefilesync...?
    - quelle fréquence de synchronisation ?
    - mieux définir ce que contient PermaFrance/DIVERS et /BIG_DATA
```

### 3.3.2 - Le dossier `/PermaFRANCE/CAMPAGNES`

Tout ce qui permet de documenter les campagnes de terrain et la collecte des données brutes, leurs traitements. Le dossier est organisé par année, site par site, date par date et méthode par méthode : `/PermaFRANCE/CAMPAGNES/YYYY/SITE/DD_MMMM_YYYY/METHODE` (`GPS`, `UTL`, `DRONE`, ...)

```         
A faire :
    - tableau synthétique des campagnes 
```

### 3.3.3 - Le dossier `/PermaFRANCE/DONNEES`

#### Dossier `/PermaFRANCE/DONNEES/1_DATA_MANAGEMENT`

```         
***A REVOIR CAR STRUCTURE CHAMBOULEE, ET A PRIORI COMPRISE DANS LE DOSSIER SUIVI PAR GIT...***
```

Contient tout ce qui permet de gérer les données, dont :

-   `/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES` = contient le descriptif des protocoles d'acquisition, de traitements et de production des séries temporelles diffusées

-   `/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/SCRIPTS` = contient les scripts `R` pour toutes sortes de manip :

    -   gestion des données de températures
    -   gestion des données de mouvement

```         
à faire : 
    - créer un fichier métadata sur les sites de mesure
    - le DMP de PermaFrance
```

Fichier de documentation de chaque fichier de données utilisé pour la consolidation des séries temporelles distribuées :

`PermaFrance/DONNEES/1_DATA_MANAGEMENT/METADATA.ods`

#### Dossier `/PermaFRANCE/DONNEES/2_DATA`

Contient les données de type *séries temporelles* à distribuer, classées par ***sites*** et par ***variables***

```         
à décider : 
    - a priori on va rapatrier toutes les données brutes-brutes dans 3_CAMPAIGNS, et ne laisser dans 2_DATA que les données diffusées
```

------------------------------------------------------------------------

# Structure des dossiers

## Gestion des données

### Documentation des métadonnées

### Préparation des séries temporelles

```         
A REGLER AVEC l'OSUG

* [ ] Comment utiliser les données qui sont sur le Nextcloud OSUG ?
* [ ] Est-ce que le dossier Git doit être synchronisé avec le Gitlab ?
* [ ] Comment gérer le fait que Flo et moi bossons sur des OS différents ?
```

Page à effacer : <https://www.osug.fr/l-observation/les-services-d-observation-a-l-osug/observatoires-ateliers/permafrance>, accédée depuis <https://cryobsclim.osug.fr/spip.php?article20>
