# Journal de suivi du stage d'Alexis Mayeux

## 13 juin 2023

J'ai créé un tableau `C:\Users\LENOVO\Desktop\STAGE XAVIER\GITLAB\managing-data\GESTION\stage_alexis\Tableaux\Suivi_deplacements_donnees_gps.xlsx`

Très bien !

### Travail sur le tableau PermaFrance_Campagnes

Hésitation sur la présence de la donnée GPS base traitée 2020 (matériel) sur le site LOU. Elle n'est pas présente dans le dossier brute. A confirmer ici : `Z:\PermaFRANCE\DONNEES\2_DATA\1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS\TRAITEES\2020`

Hésitation sur la présence de la donnée GPS base traitée 2021 (matériel) sur le site LOU. Elle n'est pas présente dans le dossier brute. A confirmer ici : `Z:\PermaFRANCE\DONNEES\2_DATA\1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS\TRAITEES\2021`

Hésitation sur la présence de la donnée GPS base traitée 2022 (matériel) sur le site LOU. Elle n'est pas présente dans le dossier brute. A confirmer ici : `Z:\PermaFRANCE\DONNEES\2_DATA\1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS\TRAITEES\2022`

## 14 juin 2023

### Progression sur tableau PermaFrance_Campagnes

Qui est l'opérateur de la campagne sur le site Iseran au 08/07/2022 ? XB

Qui est l'opérateur des campagnes sur le site du Derochoir entre le 09/09/2009 (début d'étude sûrement) au 14/10/2015 ? C'est ensuite Thomas Echelard qui est indiqué 2020. xb

Hésitation sur la présence de la donnée brute du GPS BASE pour le site du Derochoir au 14/10/2015 : `1_SITES_PERMAFRANCE\DEROCHOIR\MOUVEMENT\GPS\BRUTES\\2015`

La donnée GPS BASE est manquante dans la compilation unique pour la campagne du 15/09/2020 sur le site DEROCHOIR : `1_SITES_PERMAFRANCE\DEROCHOIR\MOUVEMENT\GPS\TRAITEES`

Pour le site de Bellecombe, est ce bien Jean Michel Krysiecki qui est l'opérateur des campagnes comprises entre celle du 31/08/2009 au 28/09/2011 ? Voir compilation unique : `1_SITES_PERMAFRANCE\BELLECOMBE\MOUVEMENT\GPS\TRAITEES`

Quel est l'institut de Jean Michel Krysiecki ? SAGE

Pour le site de Bellecombe, les données brutes (2016,2018,2019) sont inscrites comme inutilisables donc pas de post-traitement, pas de compilation et de compilation unique, il faut tout de même les inscrire dans le tableau ? : `1_SITES_PERMAFRANCE\BELLECOMBE\MOUVEMENT\GPS\BRUTES`

Pour l'ensemble des données du site Farneireta, qui est l'opérateur et quel est son institut ? : `1_SITES_PERMAFRANCE\FARNEIRETA\MOUVEMENT\GPS`

Pour le site de Fournache, qui est l'opérateur et quel est son institut ? (aucune donnée dans le dossier données) : `CAMPAGNES\2022\FOURNACHE\06_octobre_2022\GPS\RAW DATA_GEO7X_PACTE`

## 15 juin 2023

### Travail sur l'organisation des données brutes GPS entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry

Tableau synthétisant le suivi des données : \`STAGE XAVIER\\GITLAB\\managing-data\\GESTION\\stage_alexis\\Tableaux\`. Enregistré sous le nom "Suivi_deplacement_donnees_gps" sous le format .xlsx

Site Bellecombe :

-   BELLECOMBE : Le site "2 ALPES_BELLECOMBE" devient "BELLECOMBE" pour le dossier données serveur et le DDE CHAMBERY : `DONNEES\2_DATA\1_SITES_PERMAFRANCE\BELLECOMBE`

-   2007 -\> OK pour le serveur et le DDE

-   2008 -\> OK pour le serveur et le DDE

-   2009 -\> OK pour le serveur et le DDE

-   2010 -\> OK pour le serveur et le DDE

-   2011 -\> OK pour le serveur et le DDE

-   2016 -\> OK pour le serveur et le DDE

-   2018 -\> OK pour le serveur et le DDE

-   2020 -\> OK pour le serveur et le DDE

Site Derochoir :

-   2009 -\> OK pour le serveur et le DDE

-   2010 -\> OK pour le serveur et le DDE

-   2013 -\> OK pour le serveur et le DDE

-   2015 -\> OK pour le serveur et le DDE

-   2019 -\> OK pour le serveur et le DDE

-   Données traitées initialement présentes dans le dossier données brutes : :`1_SITES_PERMAFRANCE\DEROCHOIR\MOUVEMENT\GPS`ont été supprimées puis déplacées à leur place, dans le dossier données traitées :`1_SITES_PERMAFRANCE\DEROCHOIR\MOUVEMENT\GPS\TRAITEES\03_OCTOBRE_2019` pour le serveur et le DDE CHAMBERY

-   Données traitées initialement présentes dans le dossier campagnes : `\PermaFRANCE\CAMPAGNES\2020\DEROCHOIR` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `DONNEES\2_DATA\1_SITES_PERMAFRANCE\DEROCHOIR\MOUVEMENT\GPS\TRAITEES\2020` pour le serveur et le DDE CHAMBERY

-   2020 -\> OK pour le serveur et le DDE

Site Farneireta :

-   Données traitées initialement présentes dans le dossier campagnes : `PermaFRANCE\CAMPAGNES\2019\FARNEIRETA` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `DONNEES\2_DATA\1_SITES_PERMAFRANCE\FARNEIRETA\MOUVEMENT\GPS\TRAITEES\2019` pour le serveur et le DDE CHAMBERY

-   2019 -\> OK pour le serveur et le DDE

-   Données traitées initialement présentes dans le dossier campagnes : `PermaFRANCE\CAMPAGNES\2020\FARNEIRETA\GPS` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `DONNEES\2_DATA\1_SITES_PERMAFRANCE\FARNEIRETA\MOUVEMENT\GPS\TRAITEES\2020` pour le serveur et le DDE CHAMBERY

-   Données traitées initialement présentes dans le dossier données brutes : `DONNEES\2_DATA\1_SITES_PERMAFRANCE\FARNEIRETA\MOUVEMENT\GPS\BRUTES\2020` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `DONNEES\2_DATA\1_SITES_PERMAFRANCE\FARNEIRETA\MOUVEMENT\GPS\TRAITEES\2020` pour le serveur et le DDE CHAMBERY

-   2020 -\> OK pour le serveur et le DDE

### 16 juin 2023

-   Données traitées intialement présentes dans le dossier campagnes : `CAMPAGNES\2021\FARNEIRETA\08_SEPTEMBRE_2021\GPS` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `1_SITES_PERMAFRANCE\FARNEIRETA\MOUVEMENT\GPS\TRAITEES\2021` pour le serveur et le DDE CHAMBERY

-\> Mail envoyé à Emmanuel Thibert

-\> Note sur le Cloud complétée

Site Iseran :

-   2015 -\> OK pour le serveur et le DDE

-   Données traitées initialement présentes dans le dossier campagnes : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\BRUTES\2018` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\TRAITEES\2018` pour le serveur et le DDE CHAMBERY

-   2018 -\> OK pour le serveur et le DDE

-   Données traitées initialement présentes dans le dossier campagnes : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\BRUTES\2020` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\TRAITEES\2020`pour le serveur et le DDE CHAMBERY

-   2020 -\> OK pour le serveur et le DDE

Site Lou :

-   2016 -\> OK pour le serveur et le DDE

-   2017 -\> OK pour le serveur et le DDE

-   Année 2017 : Données brutes différentes entre le serveur et le DDE chambéry, un dossier est en plus dans le dossier campagnes du DDE nommé "RGP-Moda-MODANE" : `CAMPAGNES\2017\LOU\12_AOUT_2017\GPS`

-   Pour l'année 2018, le site "COL DE LOU" a été renommé "LOU" dans le dossier campagnes du serveur et du DDE

-   2018 : OK pour le serveur et le DDE CHAMBERY

-   Année 2018 : Données brutes intialement présentes dans le dossier campagnes : `1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS\BRUTES` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS\TRAITEES` pour le serveur et le DDE CHAMBERY

-   Année 2019 : OK pour le serveur et le DDE

-   Données traitées initialement présentes dans le dossier données brutes : `1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS` ont été supprimées puis déplacées à leur place, dans le dossier données traitées : `1_SITES_PERMAFRANCE\LOU\MOUVEMENT\GPS\TRAITEES\\2019`

## 19 juin 2023

### Travail sur l'organisation des données brutes TEMPERATURE entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry

-\> Création d'un tableau de suivi des données de température : `managing-data\GESTION\stage_alexis\Tableaux`. Enregistré sous le nom "Suivi_deplacements_donnees_temperature" sous le format .xslx

-\> Questionnement autour des formats des données de températures, découverte de nouveaux formats

## 20 juin 2023

### Travail sur l'organisation des données brutes TEMPERATURE entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry

-\> Discussion avec Xavier autour de l'architecture des dossiers et de la différenciation des données brutes/traitées

Bellecombe :

-   Année 2008 -\> BTS OK pour le serveur et le DDE

-   Année 2009 -\> BTS OK pour le serveur et le DDE

-   Année 2011 -\> BTS OK pour le serveur et le DDE

-   Année 2010 -\> FORAGE AMONT OK pour le serveur et le DDE

-   Année 2020 -\> FORAGE AMONT OK pour le serveur et le DDE

-   Année 2011 -\> FORAGE AMONT OK pour le serveur et le DDE

-   Année 2012 -\> FORAGE AMONT OK pour le serveur et le DDE

Informations complémentaires :

-   Pour les données brutes BTS du site Bellecombe en 2008, la date des fichiers ne correspond pas à la date de la campagne pour les données GPS. Erreur ou deux campagnes différentes ? : `CAMPAGNES\2008\BELLECOMBE\30_SEPTEMBRE_2008\BTS`

-   Pour les données brutes BTS du site Bellecombe en 2009, seulement l'année est indiquée dans le fichier . Je l'ai renseigné dans le dossier campagnes comme étant la même campagne que pour le GPS : `CAMPAGNES\2009\BELLECOMBE\31_AOUT_2009\BTS`

-   Pour les données brutes BTS du site Bellecombe en 2011, seulement l'année est indiquée dans le fichier . Je l'ai renseigné dans le dossier campagnes comme étant la même campagne que pour le GPS : `CAMPAGNES\2011\BELLECOMBE\28_SEPTEMBRE_2011\BTS`

-   Pour les données brutes Forage du site Bellecombe, le transfert des données se fera à partir du dossier : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages` qui est le plus récent et qui comporte des données supplémentaires que le dossier : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Donnees`

-   2010, Bellecombe : La date de fin de mesure du forage amont (17 aout 2010) ne correspond pas à la date du dossier (23 aout 2010) : `CAMPAGNES\2010\BELLECOMBE\23_AOUT_2010\FORAGE\FORAGE_AMONT` -\> La date conservée pour le dossier est celle du 17 aout (dernière mesure)

-   2020, Bellecombe : La date (DD_MOIS_YYYY) conservé pour le dossier campagnes : `CAMPAGNES\2020\BELLECOMBE\23_SEPTEMBRE_2020`est celle de la dernière mesure sur la série 2010-2020 du dossier données brutes : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage amont\Loggers Paratronic\Données brutes\2010-2020`

-   2020, Bellecombe : La date (DD_MOIS_YYYY) conservé pour le dossier campagnes : `CAMPAGNES\2020\BELLECOMBE\23_SEPTEMBRE_2020`est celle de la dernière mesure sur la série 2010-2020 du dossier données brutes : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage amont\Loggers Paratronic\Données brutes\2010-2020`

-   2011, Bellecombe : La date (DD_MOIS_YYYY) conservé pour le dossier campagnes : `CAMPAGNES\2011\BELLECOMBE\29_JUILLET_2011\FORAGE\FORAGE_AMONT`est celle de la dernière mesure sur la série 2011-31 juillet du dossier données brutes : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage amont\Loggers Paratronic\Données brutes\2011-31 juillet`

-   2020, Bellecombe : Les données Forage de 2018 à 2020 : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage amont\Loggers Paratronic\Données brutes\2018.01.01 - 2020.09.23` sont déjà comprises dans le dossier `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage amont\Loggers Paratronic\Données brutes\2010-2020` donc aucun transfert n'a été réalisé (doublon)

Fin de journée : Note réalisée sur le NextCloud pour le 19 et le 20/06

## 21 juin 2023

### Travail sur l'organisation des données brutes TEMPERATURE entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry + Demande de Antoine Blanc

-\> Discussion et réalisation de la demande de Antoine Blanc. Tableau ROGP récapitulatif visible ici `STAGE XAVIER\GITLAB\managing-data\GESTION\stage_alexis\Tableaux`

-\> Suite du transfert des données pour les sites :

Bellecombe :

-   Année 2010 -\> FORAGE AVAL OK pour le serveur et le DDE CHAMBERY

-   Année 2020 -\> FORAGE AVAL OK pour le serveur et le DDE CHAMBERY

-   Année 2011 -\> FORAGE AVAL OK pour le serveur et le DDE CHAMBERY

-   Année 2012 -\> FORAGE AVAL OK pour le serveur et le DDE ChHAMBERY

-   Année 2021 -\> FORAGE AVAL OK pour le serveur et le DDE CHAMBERY

Informations complémentaires :

-   2010, Bellecombe : La date (DD_MOIS_YYYY) conservée pour le dossier campagnes : `CAMPAGNES\2011\BELLECOMBE\17_AOUT_2010\FORAGE\FORAGE_AMONT` est celle de la dernière mesure sur la série 2011-31 juillet du dossier données brutes : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage amont\Loggers Paratronic\Données brutes\2010-23 aout`

-   2020, Bellecombe : La date (DD_MOIS_YYYY) conservée pour le dossier campagnes : est celle du forage amont car aucune date de mesure pour 2020 n'a été trouvé, la date sera le 23 septembre 2020 :`1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\FORAGE\Données Forages\Forage aval\Loggers Paratronic\Données brutes\\2010-2020\`

Fin de journée : Note réalisée pour le 21/06

## 22 juin 2023

### Travail sur l'organisation des données brutes TEMPERATURE entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry + Demande de Antoine Blanc

Matinée : Travail sur la demande de Antoine Blanc

-\> Suite du transfert des données pour les sites :

Bellecombe :

-   2008 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2009 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2010 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2011 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2013 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2015 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2018 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2021 -\> GST OK pour le serveur et le DDE CHAMBERY

Dérochoir :

-   2010 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2011 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2012 -\> GST OK pour le serveur et le DDE CHAMBERY

Informations complémentaires :

-   2010, Bellecombe : Des données de forages sont présentes dans les données brutes GST et les données brutes GST couvrent une période 2003-2004 : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\GST\2009-2010\BRUTES`. Le dossier GST campagnes est en date du 23 septembre 2010, date de mesure des données de forages : `CAMPAGNES\2010\BELLECOMBE\23_SEPTEMBRE_2010\GST`

-2011, Bellecombe : Des données .utl sont présentes dans les données brutes GST : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\GST\2010-2011\BRUTES`, le transfert des données .txt et .utl a été réalisé vers : `CAMPAGNES\2011\BELLECOMBE\28_SEPTEMBRE_2011\GST`

-   2013, Bellecombe : Des données .utl sont présentes dans les données brutes GST : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\GST\2011-2013\BRUTES`, le transfert des données .txt et .utl a été réalisé vers : `CAMPAGNES\2011\BELLECOMBE\09_OCTOBRE_2013\GST`

-   2015, Bellecombe : Des données .utl sont présentes dans les données brutes GST : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\GST\2013-2015\BRUTES`, le transfert des données .txt et .utl a été réalisé vers : `CAMPAGNES\2011\BELLECOMBE\17_JUILLET_2015\GST`

-   2018, Bellecombe : Des données .utl sont présentes dans les données brutes GST : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\GST\2015-2018\BRUTES`, le transfert des données .txt et .utl a été réalisé vers : `CAMPAGNES\2011\BELLECOMBE\05_OCTOBRE_2018\GST`

-   2021, Bellecombe : Des données .utl sont présentes dans les données brutes GST : `1_SITES_PERMAFRANCE\BELLECOMBE\TEMPERATURE\GST\2019-2021\BRUTES`, le transfert des données .txt et .utl a été réalisé vers : `CAMPAGNES\2011\BELLECOMBE\22_JUILLET_2021  \GST`

## 23 juin 2023

### Travail sur l'organisation des données brutes TEMPERATURE entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry + Demande de Antoine Blanc

Discussion avec Xavier concernant la demande de Antoine Blanc.

-\> Suite du transfert des données pour les sites :

Dérochoir :

-   2015 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2018 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2020 -\> GST OK pour le serveur et le DDE CHAMBERY

Iseran :

-   2018 -\> GST OK pour le serveur et le DDE CHAMBERY

-   2020 -\> GST OK pour le serveur et le DDE CHAMBERY

2_Alpes :

-   2011 -\> DTS OK pour le serveur et le DDE CHAMBERY

-   2013 (16 avril, 17 juillet, 12 avril) -\> DTS OK pour le serveur et le DDE CHAMBERY

-   2013 (17 juillet) -\> FORAGE OK pour le serveur et le DDE CHAMBERY

Informations complémentaires :

-   Dérochoir, 2015 : La date de dernière mesure dans les données brutes températures est en date du 04 novembre 2015 : `1_SITES_PERMAFRANCE\DEROCHOIR\TEMPERATURE\GST\2013-2015\BRUTES`, la date me parait tardive, et cela est peut être confirmé dans les données corrigées où la dernière mesure est en date du 14 octobre 2015 `1_SITES_PERMAFRANCE\DEROCHOIR\TEMPERATURE\GST\2013-2015\CORRIGEES`. La date du dossier campagne est celle du 14 octobre 2015.

-   Dérochoir, 2018 et 2020 : Données .utl présentes dans le dossier campagnes qui sont finalement les données GST. Dossier supprimé (doublon) `CAMPAGNES\2018\DEROCHOIR` et `CAMPAGNES\2020\DEROCHOIR`

-   Iseran, 2018 et 2020 : Données .utl présentes dans le dossier campagnes qui sont finalement les données GST. Dossier supprimé (doublon) `CAMPAGNES\2018\ISERAN` `CAMPAGNES\2020\ISERAN`

-   2_Alpes, 2013 (16 avril) : Données DTS aux formats inconnues et JPG transférées dans le dossier campagne : `CAMPAGNES\2013\2_ALPES\16_AVRIL_2013\DTS`

-   2_Alpes, 2013 (17 juillet, 04 decembre) : Données DTS aux formats inconnues transférées dans le dossier campagne : `CAMPAGNES\2013\2_ALPES\17_JUILLET_2013\DTS` et `CAMPAGNES\2013\2_ALPES\04_DECEMBRE_2013\DTS`

-   2_Alpes, 2014 : Données DTS non transférées, à regarder `1_SITES_PERMAFRANCE\2ALPES_3065\TEMPERATURE\FORAGE\Données DTS\Données brutes\2Alpes_3065_2014-09-11`

## 26 juin 2023

### Travail sur l'organisation des données brutes TEMPERATURE entre le dossier "Données" et le dossier "Campagne" pour le NextCloud et le DDE Chambéry

2 ALPES :

2013 -\> LOGGERS OK serveur et DDE CHAMBERY

2012 -\> LOGGERS OK serveur et DDE CHAMBERY

2011 (21 juillet) -\> LOGGERS OK serveur et DDE CHAMBERY

2011 (03 mars) -\> LOGGERS OK serveur et DDE CHAMBERY

2013 -\> UTL OK pour le serveur et le DDE CHAMBERY

2015 -\> UTL OK pour le serveur et le DDE CHAMBERY

2017 (14 septembre) -\> UTL OK pour le serveur et le DDE CHAMBERY

2017 (25 septembre) -\> UTL OK pour le serveur et le DDE CHAMBERY

LOU :

2016 (19 juillet) -\> GST OK pour le serveur et le DDE CHAMBERY

2016 (20 juillet) -\> GST OK pour le serveur et le DDE CHAMBERY

2015 -\> GST OK pour le serveur et le DDE CHAMBERY

2018 -\> GST OK pour le serveur et le DDE CHAMBERY

Informations complémentaires :

-   Dans dossier campagne `CAMPAGNES\2018\LOU\UTL`, des données de 2016 sont présentes dans le dossier "UTL", qu'en faire ? Elles ont été classées au préalable ici : `CAMPAGNES\2016\LOU`

Fin de journée : Note OK

## 27 juin 2023

### Tâches diverses autour des données GPS

Matin : Discussion avec Xavier autour de l'architecture des données GPS

Après-Midi : Comparaison des données GPS dans le dossier campagne entre le serveur et le DDE CHAMBERY pour avoir une organisation identique + organisation des données GPS dans la partie campagne pour les sites à plus faible intérêt.

Fin de journée : Note OK

## 28 juin 2023

### Tâches diverses autour des données GPS

Comparaison des données GPS dans le dossier campagne entre le serveur et le DDE CHAMBERY pour avoir une organisation identique + organisation des données GPS dans la partie campagne pour les sites à plus faible intérêt.

Suggestions pour la suite : Réaliser la même organisation dans le dossier campagne (serveur et DDE) pour les sites à plus faibles intérêts et obtenir les données manquantes.

Fin de journée : note OK

## 10 juillet 2023

### Inventaire des données drones dans le dossier BIGDATA et remplissage du tableau `C:\Users\LENOVO\Desktop\STAGE XAVIER\GITLAB\managing-data\GESTION\stage_alexis\Tableaux` qui se nomme "PermaFrance_campagnes_drones"

-   Laurichard, 26/08/2021 : Séparation des données JPG et DNG dans deux dossiers distincts `DATA_BIG\LAURICHARD\26_AOUT_2021\DRONE`

-   Lou, 20/10/2022 : Deux dossiers de données présents, l'un qui représente le site et l'autre un écroulement. Les deux dossiers représentent deux lignes dans le tableau `DATA_BIG\LOU\20_OCTOBRE_2022\DRONE`

1ère étape réalisée, le tableau est complété selon le DDE EDYTEM

## 20 juillet 2023

### Transfert de diverses données avant le terrain ISERAN

DDE EDYTEM ET GRENOBLE :

-   Transfert des données GPS de l'Iseran des campagnes de 2021 : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\TRAITEES\2021` dans la compilation unique : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\TRAITEES`

-\> Création d'un fichier .csv référence HAE à partir du .shp pour l'année 2022, fichier enregistré ici : `1_SITES_PERMAFRANCE\ISERAN\MOUVEMENT\GPS\TRAITEES\2022`

-\> Problème d'altitude pour l'ensemble des points entre 2021 et 2022, plus de 10 à 15 mètres en plus pour l'année 2022 malgré une référence commune (ellipsode) : Données à re-traitées ?
