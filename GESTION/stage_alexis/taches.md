# Taches à envisager

## Données GPS

### Déplacements des données brutes

de `PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE` vers `PermaFRANCE/CAMPAGNES`

#### Tableau de suivi

`PermaFRANCE/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/GESTION/stage_alexis/Tableaux/Suivi_deplacements_donnees_gps.xlsx`, pour lister tous les dossiers/fichiers à déplacer

> *en cours 13 juin 2023*

-   envoyer un mail à [emmanuel.thibert\@inrae.fr](mailto:emmanuel.thibert@inrae.fr){.email} pour connaître le passage du théodolite au GPS Leica

-   pour les dates des campagnes à Laurichard, voir `PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/LAURICHARD/MOUVEMENT/DATA/rg_laur_topo_NA_lambert3.ods` (équivalent COMPILATION_UNIQUE.ods) et `PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/LAURICHARD/MOUVEMENT/DATA/rg_laur_topo_NA_lambert3_velocity.ods` = fichier résultant du traitement avec `PermaFRANCE/DONNEES/2_DATA/1_SITES_PERMAFRANCE/LAURICHARD/MOUVEMENT/DATA/rg_laur_topo.Rmd` (script R) = ***time-series*** qu'on veut diffuser au final

#### Déplacement et suppression

=\> sur le serveur et sur les DDE (celui d'EDYTEM)

Pour le DDE de Grenoble (chez Xavier), il faudra faire une copie / mise à jour (dans les 2 sens), de même qu'avec le serveur

### Mise à jour

#### Chercher les données les plus récentes

Soit dans `CAMPAGNES`, soit sur les DDE, soit sur ordi Xavier (ou autres)

#### Mettre les données sur le serveur (et les DDE)

## Syncrhonisation DDE et serveur

### Mise en place

=\> une fois que les trois espaces de stockage ont été vérifiés et synchronisés manuellement (Freecommander, par ex.)

=\> passer par l'outil de synchro Nextcloud (ou routine bash) : à voir les gens du SI de l'OSUG (F. Malbet, et ... ?)

## Protocoles GPS

Fichier à rédiger : `PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/PermaFrance_protocole_RGV_rock_glaciers_velocity.md`

## Protocles terrain

`PermaFrance/DONNEES/1_DATA_MANAGEMENT/GITLAB_OSUG/managing-permafrance-data/PROTOCOLES/GESTION_TERRAINS.md`
